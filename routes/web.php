<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BiblioController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/livros', [BiblioController::class, 'lilivros']);

Route::get('/editoras', [BiblioController::class, 'editoras']);

Route::get('/autores', [BiblioController::class, 'autores']);

Route::get('/editlivros', [BiblioController::class, 'editarlivros']);

Route::get('/editautores', [BiblioController::class, 'editarautores']);

Route::get('/editeditoras', [BiblioController::class, 'editareditoras']);



