<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.gstatic.com">
<link href="https://fonts.googleapis.com/css2?family=Ubuntu&display=swap" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <title>Livros</title>
    <style>
    table{
        font-family: 'Ubuntu';
    }
    body{
        font-family: 'Ubuntu', 'sans-serif';
        background-color: white;
    }
    #col_main{
        font-weight:bold;
    }
    </style>
</head>
<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="/">Página inicial</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link" aria-current="page" href="/livros">Listagem dos Livros</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/autores">Listagem de Autores</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/editoras">Listagem de Editoras</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/editlivros">Editar Livros</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/editautores">Editar Autores</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/editeditoras">Editar Editoras</a>
        </li>
        
      </ul>
    </div>
  </div>
</nav>
    <center><h1>Livros</h1>
    <table border="2px">
    <div id="col_main">
    <tr>
    <td>ID do livro</td>
    <td>Nome do livro</td>
    <td>ID do autor</td>
    <td>ID da editora</td>
    <td>Ações</td>
    </tr>
    </div>
    @foreach($livros as $livros)
    <tr>
    <td>   {{$livros->id}} </td>
    <td>  {{$livros->nome}} </td>
    <td>  {{$livros->id_autor}} </td>
    <td>  {{$livros->id_editora}} </td>
    <td><button type="button" class="btn btn-danger">Deletar</button>
    <button type="button" class="btn btn-primary">Editar</button> 
<button type="button" class="btn btn-warning">Salvar </button>  <br><br>
    </td>
    </tr> 
    @endforeach
    </table>
    <br><br>
    <a href="/"><button>Clique aqui para voltar à página inicial</button></a>
</center>


</body>
</html>